#include <stdio.h>
#include <stdlib.h>

void swap(int* a, int* b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

int main(void)
{
	int i, j;
	int T, number_of_boxes;
	int length_of_boxes[100010];
	int width_of_boxes[100010];
	scanf("%d", &T);
	while(T--){
		int count = 0;
		scanf("%d", &number_of_boxes);
		for(i = 0; i < number_of_boxes; i++){
			scanf("%d%d", &length_of_boxes[i], &width_of_boxes[i]);
			if(length_of_boxes[i] > width_of_boxes[i])
				swap(&length_of_boxes[i], &width_of_boxes[i]);
		}
		for(j = 0; j < number_of_boxes; j++)
			for(i = 0; i < number_of_boxes; i++)
				if(length_of_boxes[j]<=length_of_boxes[i]&&width_of_boxes[j]<=width_of_boxes[i])
					count++;
		printf("%d\n", count - number_of_boxes);
	}
	return 0;
}
