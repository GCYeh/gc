#include <stdio.h>
#include <iostream>
#include <climits>
#define int_128 __uint128_t

using namespace std;

int_128 adj[101];    // adjacency matrix
const int_128 one = 1;
const int_128 zero = 0;
int Num;

int position(int_128 a)
{
	int count = 0;
	while((a & one) == zero){
		count++;
		a >>= 1;
	}
	return count;
}

void backtrack(int_128 P, int_128 X, int n)
{
	if (P == zero){
		if(X == zero)
			Num = Num > n? Num : n;
		return;
	}
	int p = position(P | X);
	int_128 Q = P & ~adj[p];

	while (Q)
	{
		int i = position(Q);
		backtrack(P & adj[i], X & adj[i], n+1);
		Q &= ~(one<<i);
		P &= ~(one<<i);
		X |= (one<<i);
	}
}

int main(void)
{
	int T;
	int n, m, i, j;
	int a, b;
	scanf("%d", &T);
	while(T--){
		Num = -1;
		scanf("%d%d", &n, &m);
		for (i = 0; i < 101; i++)
			adj[i] = ~0;
		for (i = 0; i < m; i++){
			scanf("%d%d", &a, &b);
			adj[a] &= ~(one<<b);
			adj[b] &= ~(one<<a);
		}
		for (i = 0; i < n; i++)
			adj[i] &= ~(one<<i);
		backtrack((one<<n)-1, zero, 0);
		printf("%d\n", Num);
	}
	return 0;
}


