#include <stdio.h>

int value_coin[12] = {1, 5, 10, 20, 50, 100, 200, 500, 1000, 2000}; //const value of coin

int takemin(int surplus, int num_coin[])    //function which returns how many coin should be taken away to get surplus money 
{
	if(surplus < 0)
		return 2147483600;
	int i, num;
	int min = 0;
	for(i = 9; i >= 8; i--){
		num = surplus / value_coin[i];
		if(num >= num_coin[i]){
			surplus -= num_coin[i] * value_coin[i];
			min += num_coin[i];
		}
		else{
			surplus -= num * value_coin[i];
			min += num;
		}
	}
	num = surplus / 1000;                              // case of i==7
	if(num >= (num_coin[7]/2)){
		surplus -= (num_coin[7]/2) * 1000;
		min += (num_coin[7]/2) * 2;
	}
	else{
		surplus -= num * 1000;
		min += 2 * num;
	}
	for(i = 6; i >= 5; i--){
		num = surplus / value_coin[i];
		if(num >= num_coin[i]){
			surplus -= num_coin[i] * value_coin[i];
			min += num_coin[i];
		}
		else{
			surplus -= num * value_coin[i];
			min += num;
		}
	}
	num = surplus / 100;                      // case of i==4
	if(num >= (num_coin[4]/2)){
		surplus -= (num_coin[4]/2) * 100;
		min += (num_coin[4]/2) * 2;
	}
	else{
		surplus -= num * 100;
		min += 2 * num;
	}
	for(i = 3; i >= 0; i--){
		num = surplus / value_coin[i];
		if(num >= num_coin[i]){
			surplus -= num_coin[i] * value_coin[i];
			min += num_coin[i];
		}
		else{
			surplus -= num * value_coin[i];
			min += num;
		}
	}
	if(surplus != 0)           //cannot get surplus money by given coin
		return 2147483600;
	return min;
}

int main(void)
{
	int T, i;
	int min;
	int price;
	int total_money;
	int total_coin;
	int surplus;
	int num_coin[12];
	int case_coin[4];
	scanf("%d", &T);
	while(T--){               //number of cases
		total_money = 0;      //initialize
		total_coin = 0;
		scanf("%d", &price);
		for(i = 0; i < 10; i++){
			scanf("%d", &num_coin[i]);
			total_money += num_coin[i] * value_coin[i];
			total_coin += num_coin[i];
		}
		surplus = total_money - price;        //surplus to be taken away
		if(surplus < 0){
			printf("-1\n");
			continue;
		}
		if(!surplus){
			printf("%d\n", total_coin);
			continue;
		}
		min = 2147483600;
		if(num_coin[4] && num_coin[7]){                   // one of the 4 posibilities contains the best way (least number of coins) to take away the coins
			case_coin[0] = takemin(surplus, num_coin);
			num_coin[4]--;
			case_coin[1] = takemin(surplus-50, num_coin) + 1;
			num_coin[7]--;
			case_coin[2] = takemin(surplus-550, num_coin) + 2;
			num_coin[4]++;
			case_coin[3] = takemin(surplus-500, num_coin) + 1;
			for(i = 0; i < 4; i++)
				min = min < case_coin[i]? min:case_coin[i];
		}
		else if(num_coin[4]){
			case_coin[0] = takemin(surplus, num_coin);
			num_coin[4]--;
			case_coin[1] = takemin(surplus-50, num_coin) + 1;
		        min = case_coin[0] < case_coin[1]? case_coin[0]:case_coin[1];
		}
		else if(num_coin[7]){
			case_coin[0] = takemin(surplus, num_coin);
			num_coin[7]--;
			case_coin[1] = takemin(surplus-500, num_coin) + 1;
			min = case_coin[0] < case_coin[1]? case_coin[0]:case_coin[1];
		}
		else
			min = takemin(surplus, num_coin);
		if(min > 2e9)   
			printf("-1\n");
		else
			printf("%d\n", total_coin - min);
	}
	return 0;
}

