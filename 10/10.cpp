#include <iostream>
#include <stdio.h>
#include <stack>
using namespace std;

int main(void)
{
	int T, n;
	int a[100010];
	int b[100010];
	int i, j;
	int max;
	int count;
	scanf("%d", &T);
	stack<int> mystack;
	while(T--){
		scanf("%d", &n);
		max = -1;
		count = n;
		for(i = 0; i < n; i++){
			scanf("%d", &a[i]);
			b[i] = -1;
			if(a[i] > max)
				max = a[i];
		}
		i = n-1;
		mystack.push(i);
		count*=2;
		while(count--){
			i--;
			if(i==-1)
				i+=n;
			if(mystack.empty() && a[i] != max){
				mystack.push(i);
				continue;
			}
			if(a[i] == max){
				while(!mystack.empty()){
					b[mystack.top()] = i+1;
					mystack.pop();
				}
				b[i] = 0;
				continue;
			}
			if(a[i] <= a[mystack.top()])
				mystack.push(i);
			else{
				while(!mystack.empty()){
					if(a[mystack.top()] < a[i]){
						b[mystack.top()] = i+1;
						mystack.pop();
					}
					else
						break;
				}
				mystack.push(i);
			}
		}
		while(!mystack.empty())
			mystack.pop();
		for(i = 0; i < n-1; i++)
			printf("%d ", b[i]);
		printf("%d\n", b[n-1]);
	}
	return 0;
}


			
		
