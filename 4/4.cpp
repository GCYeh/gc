#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int DP[2010][2010];                      // create a DP table 

void recur(int table1[2010][28], int table2[2010][28], int index1, int index2, int len){   //using the alphabetica table, to find the LCS betweenn the two strings in alphectically order 
	if(len == 0)
		return;
	int i;
	for(i = 0; i <= 26; i++){
		if (DP[table1[index1][i]][table2[index2][i]] == len){
			printf ("%c", i+'a');
			recur(&table1[0], &table2[0], table1[index1][i]-1, table2[index2][i]-1, len-1);
			break;
		}
	}
	return;
}

int main(void)
{
	int T, i, j;
	char s1[2010];
	char s2[2010];
	char temp[2010];
	int table1[2010][28];
	int table2[2010][28];
	scanf("%d", &T);
	while(T--){
		scanf("%s%s", s1, s2);
		for(i = 0; i < strlen(s1); i++)                                //reverse the strings
			temp[i] = s1[strlen(s1)-i-1];
		temp[strlen(s1)] = '\0';
		strcpy(s1, temp);
		for(i = 0; i < strlen(s2); i++)
			temp[i] = s2[strlen(s2)-i-1];
		temp[strlen(s2)] = '\0';
		strcpy(s2, temp);
		for(i = 0; i <= strlen(s1); i++)                          //fill in the DP table
			for(j = 0; j <= strlen(s2); j++){
				if(i == 0 || j == 0)
					DP[i][j] = 0;
				else if(s1[i-1] == s2[j-1])
					DP[i][j] = DP[i-1][j-1]+1;
				else{
					if(DP[i-1][j] < DP[i][j-1])
						DP[i][j] = DP[i][j-1];
					else
						DP[i][j] = DP[i-1][j];
				}
			}
		for(i = 0; i <= 26; i++){                 //make a table to memorize the position all the alphabets finally appears in the string
			table1[0][i] = 0;
			table2[0][i] = 0;
		}
		int temparray[28] = {0};
		for(i = 0; i <= strlen(s1); i++){                   //fill in the table
			for(j = 0; j <= 26; j++)
				table1[i][j] = temparray[j];
			table1[i][s1[i-1] - 'a'] = i;
			temparray[s1[i-1] - 'a'] = i; 
		}
		for(i = 0; i <= 26; i++)
			temparray[i] = 0;
		for(i = 0; i <= strlen(s2); i++){                   
			for(j = 0; j <= 26; j++)
				table2[i][j] = temparray[j];
		       table2[i][s2[i-1] - 'a'] = i;
		       temparray[s2[i-1] - 'a'] = i;
		}
		recur(&table1[0], &table2[0], strlen(s1), strlen(s2), DP[strlen(s1)][strlen(s2)]);       // recursive function to find LCS
		printf("\n");
	}
	return 0;
}

