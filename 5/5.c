#include <stdio.h>
#include <stdlib.h>
#define K 1000000007

int power(int a, int b)
{
	int i;
	int j = a;
	for(i = 1; i < b; i++)
		a *= j;
	return a;
}

int main(void)
{
	int T, n, m;
	int i, j, k;
	scanf("%d", &T);
	while(T--){
		scanf("%d%d", &n, &m);
		int m_j_1;
		int o = power(2, m);
		char b[17][17];
		int a[17][17][32800] = {{{0}}};
		for(i = 0; i < n; i++)
			for(j = 0; j < m; j++)
				scanf("%c", &b[i][j]);
		for(i = 0; i < n; i++)
			b[i][m] = 'X';
		a[0][0][0] = 1;
		for(i = 0; i < n; i++)
			for(j = 0; j < m; j++){
				m_j_1 = m - j - 1;
				if(b[i][j] == 'X'){
					for(k = 0; k < o; k++){
						if(!a[i][j][k])
							continue;
						if(j != m-1){
							a[i][j+1][k&~(1<<m_j_1)]+=a[i][j][k];
							a[i][j+1][k&~(1<<m_j_1)] %= K;
						}
						else{
							a[i+1][0][k&~(1<<m_j_1)]+=a[i][j][k];
							a[i+1][0][k&~(1<<m_j_1)] &= K;
						}
					}
				}
				else{
					for(k = 0; k < o; k++){
						if(!a[i][j][k])
							continue;
						if(j != m-1){
							a[i][j+1][k]+=a[i][j][k];
							a[i][j+1][k] %= K;
						}
						else{
							a[i+1][0][k]+=a[i][j][k];
							a[i][j+1][k] %= K;
						}
						if(b[i][j+1] != 'X'){
							if(j != m-2){
								a[i][j+2][k]+=a[i][j][k];
								a[i][j+1][k] %= K;
							}
							else{
								a[i+1][0][k]+=a[i][j][k];
								a[i+1][0][k] %= K;
							}
						}
						if(b[i+1][j] != 'X'){
							if(j != m-1){
								a[i][j+1][k|1<<m_j_1]+=a[i][j][k];
								a[i][j+1][k|1<<m_j_1] %= K;
							}
							else{
								a[i+1][0][k|1<<m_j_1]+=a[i][j][k];
								a[i+1][0][k|1<<m_j_1] %= K;
							}
						}
						if(b[i][j+1] != 'X' && b[i+1][j] != 'X' && b[i+1][j+1] != 'X'){
							if(j != m-2){
								a[i][j+2][k|(1<<m_j_1+1<<(m_j_1-1))]+=a[i][j][k];
								a[i][j+1][k|(1<<m_j_1+1<<(m_j_1-1))] %= K;
							}
							else{
								a[i+1][0][k|(1<<(m-j-3)+1<<(m-j-4))]+=a[i][j][k];
								a[i+1][0][k|(1<<(m-j-3)+1<<(m-j-4))] %= K;
							}
						}
					}
				}
			}
		printf("%d\n", a[n][0][0]);
	}
	return 0;
}

						


